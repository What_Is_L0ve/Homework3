﻿using Common;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Homework;

internal static class Program
{
    private static void Main()
    {
        var provider = DependencyInjection.ConfigureServices();
        var service = provider.GetRequiredService<IGuessNumberService>();
        service.Process();
    }
}