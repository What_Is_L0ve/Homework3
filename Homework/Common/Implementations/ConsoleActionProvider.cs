﻿using Common.Interfaces;

namespace Common.Implementations;

public class ConsoleActionProvider : IConsoleActionProvider
{
    public void SetForegroundColor(ConsoleColor color)
    {
        Console.ForegroundColor = color;
    }

    public void ResetForegroundColor()
    {
        Console.ResetColor();
    }

    public void Clear()
    {
        Console.Clear();
    }

    public void Beep()
    {
        Console.Beep();
    }
}