﻿using Common.Interfaces;

namespace Common.Implementations;

public class InputIntNumberConsoleBeepProvider : InputIntNumberConsoleProvider
{
    private readonly IConsoleActionProvider _actionProvider;

    public InputIntNumberConsoleBeepProvider(IStringReader reader, IStringParser<int> parser,
        IStringWriter writer, IConsoleActionProvider actionProvider) : base(reader, parser, writer)
    {
        _actionProvider = actionProvider;
    }

    public override int Input(string hint)
    {
        _actionProvider.Beep();
        var result = base.Input(hint);

        return result;
    }
}