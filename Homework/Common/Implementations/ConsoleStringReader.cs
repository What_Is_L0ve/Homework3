﻿using Common.Interfaces;

namespace Common.Implementations;

public class ConsoleStringReader : IStringReader
{
    string IStringReader.Read()
    {
        return Console.ReadLine() ?? string.Empty;
    }
}