﻿using Common.Interfaces;

namespace Common.Implementations;

public class InputIntNumberConsoleProvider : IInputNumberProvider<int>
{
    private readonly IStringParser<int> _parser;
    private readonly IStringReader _reader;

    private readonly IStringWriter _writer;

    public InputIntNumberConsoleProvider(IStringReader reader, IStringParser<int> parser, IStringWriter writer)
    {
        _reader = reader;
        _parser = parser;
        _writer = writer;
    }

    public virtual int Input(string hint)
    {
        _writer.Write(hint);
        var str = _reader.Read();

        return _parser.Parse(str);
    }
}