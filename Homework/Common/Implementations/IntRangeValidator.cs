﻿using Common.Interfaces;

namespace Common.Implementations;

public class IntRangeValidator : IRangeValidator<int>
{
    public bool Validate(int startValue, int endValue, int attemptCount)
    {
        return startValue >= default(int)
               && endValue > startValue
               && attemptCount >= 1;
    }
}