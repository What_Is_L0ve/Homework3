﻿using Common.Interfaces;

namespace Common.Implementations;

public class StringToIntParser : IStringParser<int>
{
    public int Parse(string s)
    {
        return int.TryParse(s, out var value)
            ? value
            : default;
    }
}