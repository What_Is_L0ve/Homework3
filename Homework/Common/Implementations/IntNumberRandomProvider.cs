﻿using Common.Interfaces;

namespace Common.Implementations;

public class IntNumberRandomGenerator : IIntNumberRandomGenerator
{
    private readonly Random _random;

    public IntNumberRandomGenerator()
    {
        _random = new Random();
    }

    public int Get(int minValue, int maxValue)
    {
        return _random.Next(minValue, maxValue);
    }
}