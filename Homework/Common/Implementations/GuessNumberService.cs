﻿using Common.Interfaces;

namespace Common.Implementations;

public class GuessNumberService : IGuessNumberService
{
    private readonly IConsoleActionProvider _consoleProvider;
    private readonly IInputNumberProvider<int> _inputNumberProvider;
    private readonly IIntNumberRandomGenerator _randomGenerator;
    private readonly IStringReader _reader;
    private readonly IRangeValidator<int> _validator;
    private readonly IStringWriter _writer;

    public GuessNumberService(IInputNumberProvider<int> inputNumberProvider,
        IRangeValidator<int> validator, IIntNumberRandomGenerator randomGenerator,
        IStringReader reader,
        IConsoleActionProvider consoleProvider, IStringWriter writer)
    {
        _inputNumberProvider = inputNumberProvider;
        _validator = validator;
        _randomGenerator = randomGenerator;
        _reader = reader;
        _consoleProvider = consoleProvider;
        _writer = writer;
    }

    public void Process()
    {
        int startValue;
        int endValue;
        int attemptCount;
        do
        {
            _consoleProvider.Clear();
            startValue = _inputNumberProvider.Input("Введите начальное число: ");
            endValue = _inputNumberProvider.Input("Введите конечное число: ");
            attemptCount = _inputNumberProvider.Input("Введите количество попыток: ");

            if (_validator.Validate(startValue, endValue, attemptCount))
            {
                break;
            }
        } while (!_validator.Validate(startValue, endValue, attemptCount));

        var number = _randomGenerator.Get(startValue, endValue);
        var attempt = 0;

        while (true)
        {
            attempt++;
            var inputNumber = _inputNumberProvider.Input("Введите ваше число: ");
            if (number < inputNumber)
            {
                _writer.Write($"Меньше, попытка {attempt}");
                if (attempt == attemptCount)
                {
                    _writer.Write($"Попытки закончились, вы проиграли. Загаданное число {number}");
                    break;
                }
            }
            else if (number > inputNumber)
            {
                _writer.Write($"Больше, попытка {attempt}");
                if (attempt == attemptCount)
                {
                    _writer.Write($"Попытки закончились, вы проиграли. Загаданное число {number}");
                    break;
                }
            }
            else if (number == inputNumber)
            {
                _writer.Write($"Вы угадали число {number} с {attempt} попытки из {attemptCount}");

                break;
            }
        }

        _reader.Read();
    }
}