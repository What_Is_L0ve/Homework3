﻿using Common.Interfaces;

namespace Common.Implementations;

public class InputIntNumberConsoleGreenProvider : InputIntNumberConsoleProvider
{
    private const ConsoleColor Color = ConsoleColor.Green;

    private readonly IConsoleActionProvider _actionProvider;

    public InputIntNumberConsoleGreenProvider(IStringReader reader, IStringParser<int> parser,
        IStringWriter writer, IConsoleActionProvider actionProvider) : base(reader, parser, writer)
    {
        _actionProvider = actionProvider;
    }

    public override int Input(string hint)
    {
        _actionProvider.SetForegroundColor(Color);
        var result = base.Input(hint);
        _actionProvider.ResetForegroundColor();

        return result;
    }
}