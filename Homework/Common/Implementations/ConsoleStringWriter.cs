﻿using Common.Interfaces;

namespace Common.Implementations;

public class ConsoleStringWriter : IStringWriter
{
    public void Write(string text)
    {
        Console.WriteLine(text);
    }
}