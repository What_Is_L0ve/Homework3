﻿namespace Common.Interfaces;

public interface IScreenAction
{
    void Clear();
}