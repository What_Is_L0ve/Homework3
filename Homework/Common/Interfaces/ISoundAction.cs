﻿namespace Common.Interfaces;

public interface ISoundAction
{
    void Beep();
}