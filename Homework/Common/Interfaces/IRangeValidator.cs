﻿namespace Common.Interfaces;

public interface IRangeValidator<in T>
{
    bool Validate(T startValue, T endValue, T attemptCount);
}