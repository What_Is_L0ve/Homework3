﻿namespace Common.Interfaces;

public interface IInputNumberProvider<out T>
{
    T Input(string hint);
}