﻿namespace Common.Interfaces;

public interface IStringReader
{
    string Read();
}