﻿namespace Common.Interfaces;

public interface IIntNumberRandomGenerator
{
    int Get(int minValue, int maxValue);
}