﻿namespace Common.Interfaces;

public interface IStringParser<out T>
{
    T Parse(string s);
}