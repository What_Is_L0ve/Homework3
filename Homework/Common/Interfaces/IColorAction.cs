﻿namespace Common.Interfaces;

public interface IColorAction
{
    void SetForegroundColor(ConsoleColor color);

    void ResetForegroundColor();
}