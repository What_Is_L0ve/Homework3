﻿namespace Common.Interfaces;

public interface IStringWriter
{
    void Write(string text);
}