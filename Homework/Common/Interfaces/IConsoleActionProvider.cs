﻿namespace Common.Interfaces;

public interface IConsoleActionProvider : IColorAction, ISoundAction, IScreenAction
{
}