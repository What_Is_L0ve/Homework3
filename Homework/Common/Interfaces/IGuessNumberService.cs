﻿namespace Common.Interfaces;

public interface IGuessNumberService
{
    void Process();
}