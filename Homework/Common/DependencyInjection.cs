﻿using Common.Implementations;
using Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Common;

public static class DependencyInjection
{
    public static IServiceProvider ConfigureServices()
    {
        var services = new ServiceCollection();
        services.AddSingleton<IStringWriter, ConsoleStringWriter>();
        services.AddSingleton<IStringReader, ConsoleStringReader>();
        services.AddSingleton<IStringParser<int>, StringToIntParser>();
        services.AddSingleton<IRangeValidator<int>, IntRangeValidator>();
        services.AddSingleton<IIntNumberRandomGenerator, IntNumberRandomGenerator>();
        services.AddSingleton<IConsoleActionProvider, ConsoleActionProvider>();
        services.AddSingleton<IInputNumberProvider<int>, InputIntNumberConsoleGreenProvider>();
        services.AddSingleton<IGuessNumberService, GuessNumberService>();

        return services.BuildServiceProvider();
    }
}